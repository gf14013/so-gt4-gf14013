#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){

	int cantidad;

	typedef struct{
		char nombre[40];
		int dui;
		float sueldo;
	}empleado;

	empleado *arreglo[1000];
	empleado *b;
	printf("Cuantos registros desea llenar?");
	scanf("%d", &cantidad);

	for(int i=0;i<cantidad;i++){
		printf("Ingrese el nombre del empleado %d\t", i+1);
		arreglo[i]=(empleado *) malloc(cantidad*sizeof(empleado));
		scanf("%40s", &arreglo[i]->nombre);
		printf("Ingrese el numero de DUI del empleado %d\t", i+1);
		scanf("%d", &arreglo[i]->dui);
		printf("Ingrese el sueldo del empleado %d\t", i+1);
		scanf("%f", &arreglo[i]->sueldo);
	}
	
	printf("*****************Ordenado por nombre******************\n");
	for (int i=0; i<cantidad; i++){
		for(int j=0; j<cantidad-1; j++){
			if(strcmp(arreglo[j]->nombre, arreglo[j+1]->nombre)>0){
				b=arreglo[j];
				arreglo[j]=arreglo[j+1];
				arreglo[j+1]=b;
			}
		}
	}

	for(int i=0;i<cantidad;i++){
		printf ("%s, %d, %.2f\n",arreglo[i]->nombre, arreglo[i]->dui, arreglo[i]->sueldo );

	}
	
	float suma=0;
	printf("*****************Ordenado por sueldo******************\n");
	for (int i=0; i<cantidad; i++){
		for(int j=0; j<cantidad-1; j++){
			if(arreglo[j]->sueldo < arreglo[j+1]->sueldo){
				b=arreglo[j];
				arreglo[j]=arreglo[j+1];
				arreglo[j+1]=b;
			}
		}
	suma=(suma+arreglo[i]->sueldo);
	}
	suma=suma/cantidad;

	for(int i=0;i<cantidad;i++){
		printf ("%s, %d, %.2f\n",arreglo[i]->nombre, arreglo[i]->dui, arreglo[i]->sueldo );

	}
	printf("...............\n el promedio de los sueldos es:%.2f\n", suma);

return 0;
}

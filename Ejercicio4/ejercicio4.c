#include <stdio.h>
#include <stdlib.h>

int main(){

	int *pnumero;
	int num1, num2;
	char *pchar;
	char letra1;
	num1 = 2;
	num2 = 5;
	letra1 = 'a';

	pnumero = &num1; 

	printf("El valor de pnumero es:%p\n ", pnumero); 
	printf("El valor contenido en pnumero es:%d\n ", *pnumero);
	printf("Los valores de num1 y num2 son:%d,%d\n ", num1, num2);
	printf("El valor de letra1 es; %c\n", letra1);  
}

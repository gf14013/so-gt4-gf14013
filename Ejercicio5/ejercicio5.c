
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int ordena(int puntero[], int elementos){

	for (int i = 0; i < elementos - 1; i++){
		for (int x = i + 1; x < elementos; x++){
			int aux=0;
			if(puntero[i]<puntero[x]){
				aux=puntero[i];
				puntero[i]=puntero[x];
				puntero[x]=aux;
			}
		}
	}
}

int imprimi(int puntero[], int elementos){

	for (int j=0;j<elementos;j++){
		printf("%d\n",puntero[j]);
	}

}




int main (){

	int *puntero;
	int elementos;
	

	printf("Ingrese el número de elementos a operar");
	scanf("%d", &elementos);

	puntero=(int*)malloc(elementos);
	
	srand (time(NULL));
	for(int i = 0 ; i < elementos; i++){
		puntero[i] = rand()%100;
	}
	
	ordena(puntero, elementos);

	imprimi(puntero, elementos); 
	return 0;

}


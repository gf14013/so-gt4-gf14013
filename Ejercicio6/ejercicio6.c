#include <stdio.h>
#include <stdlib.h>

int llenado(int puntero[], int elementos){
	for (int i=0;i<elementos;i++){
		printf("Ingrese un numero entero positivo...\n");
		scanf("%d",&puntero[i]);
	}
}

int calcular(int puntero[], int elementos){

	int b;
	float sum=0;
	for (int i=0; i<elementos; i++){
		for(int j=0; j<elementos-1;j++){
			if(puntero[j]<puntero[j+1]){
				b=puntero[j];
				puntero[j]=puntero[j+1];
				puntero[j+1]=b;
			}
		}
		sum=(sum+puntero[i]);
	}
	sum=sum/elementos;
	printf("El numero Maximo es: %d\n", puntero[0]);
	printf("El numero Minimmo es: %d\n", puntero[elementos-1]);
	printf("El Promedio es: %2.2f\n ", sum);

}

int main (){

	int *puntero;
	int elementos;
	
	printf("Ingrese el numero de elementos a operar...\n");
	scanf("%d", &elementos);
	
	puntero = (int *) malloc(elementos);
	
	llenado(puntero,elementos);
	calcular(puntero, elementos);
	return 0; 

}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){

	int cantidad;
	
	printf("Cual es el grado del Polinomio: \n");
	scanf("%d",&cantidad);

	double *puntero1, *puntero2;

	puntero1=(double*) malloc(cantidad*sizeof(double));
	puntero2=(double*) malloc(cantidad*sizeof(double));
	
	printf("Digite los coeficientes \n");
	for(int i=0;i<cantidad;i++){
		scanf("%lf",&puntero1[i]);
	}

	double x;
	printf("De cuanto será el valor de x \n");
	scanf("%lf", &x);

	for(int i=0;i<cantidad;i++){
		puntero2[i]=puntero1[i]*pow(x,(i+1));
	}

	printf("El grado del polinomio: %d\n resolviendo pol\n", cantidad);

	for (int i=0; i<cantidad; i++){
		printf("%2.2f\n",puntero2[i]);
	}
	return 0;
}

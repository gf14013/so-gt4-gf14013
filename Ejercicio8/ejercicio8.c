#include <stdio.h>
#include <stdlib.h>

char *pedirTexto();
void contarVocales(char*,int[]);
void imprimir (int []);

	void main(){
	
		char *texto;
		int num[5];
		texto=pedirTexto();
		contarVocales(texto, num);
		imprimir(num);
	}

	char *pedirTexto(){
		static char str[256];
		printf("Escriba un Texto:\t");
		fgets(str, 256, stdin);
		printf("......................................................................\n");
		return str;
	}

	void contarVocales(char *str, int voc[]){

		for (int i=0; i<5; i++){
			voc[i]=0;
		}
	while(*(str) !=0){ 
		switch(*(str)){
			
			case 'a':
			case 'A':
				voc[0]++;
			break;
			case 'e':
			case 'E':
				voc[1]++;
			break;
			case 'i':
			case 'I':
				voc[2]++;
			break;
			case 'o':
			case 'O':
				voc[3]++;
			break;
			case 'u':
			case 'U':
				voc[4]++;
			break;
		}
		str++;	
	}	

	}

void imprimir(int voc[]){
	printf("Se contaron %d de la vocal 'a'\n",voc[0]);
	printf("Se contaron %d de la vocal 'e'\n",voc[1]);
	printf("Se contaron %d de la vocal 'i'\n",voc[2]);
	printf("Se contaron %d de la vocal 'o'\n",voc[3]);
	printf("Se contaron %d de la vocal 'u'\n",voc[4]);
	printf("............................................................\n");

}

